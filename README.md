TumblrBackup v. 1.0beta
================================================================================

/*******************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: minimal.procedure@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

ver: 1.0.0.0beta10

Added:	Restoring post is possible with some limitation, specially with audio and video,
        only embedded post is supported at the moment.  
Added:	SWT library for 64bit (non tested)  

THE SOFTWARE I REALLY, REALLY BUGGED, USE AT OWN RISK.  

Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}  
minimal.procedure@gmail.com  
zairik@gmail.com  

Feel free to use.  

TumblrBackup <user> <localbackupfolder> [-v:--verbose]  
