/**
 * *****************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: zairik@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package org.pragmas.tumblr

import java.net.{URLConnection, URL, HttpURLConnection}
import java.io._
import java.nio.charset.Charset
import java.net.{URI, URLEncoder}
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
//import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.cookie.Cookie
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair
import org.apache.http.protocol.HTTP
import org.apache.http.util.EntityUtils
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.{FileBody, StringBody}
import org.apache.commons.lang3.StringEscapeUtils
import scala.xml._
import scala.io.Source
import scala.collection.mutable.{Queue, HashMap, ArrayBuffer, ListBuffer, StringBuilder}
import scala.io.BufferedSource
import scala.collection.JavaConversions._

import FileHelper._

class TumblrRestore(email: String, password: String, postPrivate: Boolean, postState: String, baseDir: String, interval: Int = 2000, verbose: Boolean = true) {

  val LOGGER = Logger
  LOGGER.VERBOSE = verbose

  val URL_API_WRITE = "http://www.tumblr.com/api/write"
  val generator = "Tumblr Backup ver. " + TumblrBackup.VERSION

  private var currentPostParsing = ""

  private val backupNodes: NodeSeq = openBackupNodes

  /*TODO: Stupid workaround*/
  private def ampToEnt(source: String): String = {
    val unescaped = StringEscapeUtils.unescapeHtml4(source)
    unescaped.replaceAll("&amp;", "&").replaceAll("&", "&amp;")
  }

  //  private def entToAmp(source : String) : String = {
  //    source.replaceAll("&amp;", "&")
  //  }

  private def XML_loadFile(filename: String): Elem = {
    val reader = new FileInputStream(filename)
    val buffer = new BufferedSource(reader)
    XML.loadString(ampToEnt(buffer.addString(new StringBuilder).mkString))
  }

  private def openBackupNodes: NodeSeq = {
    //val backup = XML.loadFile(baseDir + "/index.html")
    val backup = XML_loadFile(baseDir + "/index.html")
    backup \\ "dl"
  }

  private def readRegularData(metaLinks: NodeSeq, fromAnswer: Boolean = false): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.REGULAR
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          data += "body" -> ""
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "title" -> (post \ "body" \ "h1").text
          if (fromAnswer) {
            data("body") += <p><strong> Question: </strong></p>
            (post \ "body" \ "div").head.child foreach { e =>
              data("body") += e
            }
            data("body") += <p><strong> Answer: </strong></p>
            (post \ "body" \ "div").last.child foreach { e =>
              data("body") += e
            }
          } else {
            (post \ "body" \ "div").head.child foreach { e =>
              data("body") += e
            }
          }
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readQuoteData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.QUOTE
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          data += "quote" -> ""
          data += "source" -> ""
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "title" -> (post \ "body" \ "h1").text
          val divs = (post \ "body" \ "div")
          divs(0).child foreach { e => data("quote") += e }
          if (!divs(1).isEmpty) divs(1).child foreach { e => data("source") += e }
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readLinkData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.LINK
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          data += "name" -> ""
          //data += "url" -> ""
          data += "description" -> ""
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "title" -> (post \ "body" \ "h1").text
          val divs = (post \ "body" \ "div")
          if (!divs(0).isEmpty) divs(0).child foreach { e => data("name") += e }
          data += "url" -> (divs(1) \ "a" \ "@href").text
          if (!divs(0).isEmpty) divs(2).child foreach { e => data("description") += e }
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readConversationData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.CONVERSATION
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          //data += "conversation" -> ""
          val post = XML.loadFile(href)
          data += "title" -> (post \ "body" \ "h1").text
          val divs = (post \ "body" \ "div")
          data += "conversation" -> divs(0).text
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readVideoData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.VIDEO
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          data += "embed" -> ""
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "caption" -> (post \ "body" \ "h1").text
          val divs = (post \ "body" \ "div")
          if (!divs(0).isEmpty) divs(0).child foreach { e => data("embed") += e }
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readAudioData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.AUDIO
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          data += "externally-hosted-url" -> ""
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "caption" -> (post \ "body" \ "h1").text
          val divs = (post \ "body" \ "div")
          if (!divs(0).isEmpty) {
            val embed = divs(0).child(0)
            val urlString = (embed \ "@src").text
            val paramsString = urlString.slice(urlString.indexOf('?'), urlString.size - 1)
            val paramsAudioFileParam = paramsString.slice(0, urlString.indexOf('&'))
            val url = paramsAudioFileParam.split('=')(1)
            data += "externally-hosted-url" -> url
          }
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def readPhotoData(metaLinks: NodeSeq): Map[String, String] = {
    val data = new HashMap[String, String]
    data += "type" -> TumblrTags.PHOTO
    data += "format" -> "html"
    metaLinks foreach { link =>
      val href = new File(baseDir, (link \ "@href").text.tail)
      link.text match {
        case TumblrTags.CONTENT => {
          //val post = XML.loadFile(href)
          val post = XML_loadFile(href.getAbsolutePath)
          data += "caption" -> (post \ "body" \ "h1").text
          val imageNode = ((post \ "body" \ "div" \ "ul" \ "li").head \ "img" \ "@src").text
          val basePostDir = new File(baseDir, currentPostParsing)
          val imagePath = new File(basePostDir, imageNode.tail)
          data += "data" -> imagePath.toString
        }
        case TumblrTags.TAGS => {
          var tags: String = ""
          val sources = Source.fromFile(href).getLines
          sources foreach { line => tags += line + ',' }
          data += "tags" -> tags.stripSuffix(",")
        }
        case _ => ""
      }
    }
    data.toMap
  }

  private def POST(data: Map[String, String]): Unit = {
    LOGGER.message("posting...")
    val client = new DefaultHttpClient
    val httpost = new HttpPost(URL_API_WRITE)

    val parameters = new ListBuffer[NameValuePair]
    parameters += new BasicNameValuePair("email", email)
    parameters += new BasicNameValuePair("password", password)
    parameters += new BasicNameValuePair("generator", generator)
    parameters += new BasicNameValuePair("private", (if (postPrivate) "1" else "0"))
    parameters += new BasicNameValuePair("state", postState)

    var useMultipartPost = false
    var filename = ""
    var postTitle = ""
    data foreach {
      case (key, value) =>
        if (key == "title" || key == "quote") postTitle = value
        if (key == "data") {
          useMultipartPost = true
          filename = value
        } else
          parameters += new BasicNameValuePair(key, value)
    }

    if (useMultipartPost) {
      val multiPartEntity = new MultipartEntity
      val fileBody = new FileBody(new File(filename)) //, "image/jpeg")
      parameters.toList foreach { e =>
        multiPartEntity.addPart(e.getName, new StringBody(e.getValue, Charset.forName("UTF-8")))
      }
      multiPartEntity.addPart("data", fileBody)
      httpost.setEntity(multiPartEntity)
    } else {
      httpost.setEntity(new UrlEncodedFormEntity(parameters.toList, HTTP.UTF_8))
    }

    val response = client.execute(httpost)
    val entity = response.getEntity

    val statusCode = response.getStatusLine.getStatusCode
    statusCode match {
      case 201 => LOGGER.message(postTitle + " - Succesfully uploaded")
      case 403 => LOGGER.message(postTitle + " - Access forbidden")
      case 400 => LOGGER.message(postTitle + " - Bad Request")
      case _ => LOGGER.message(postTitle + " - Status code: " + statusCode)
    }
    if (entity != null) {
      EntityUtils.consume(entity)
    }
    client.getConnectionManager.shutdown
    LOGGER.message("posting done.")
  }

  private def postData(node: Node): Unit = {
    currentPostParsing = (node \ "@id").text
    var postType = currentPostParsing.split('_').last
    postType = postType.slice(1, postType.size - 1)
    postType match {
      case TumblrTags.REGULAR => if (TumblrTags.isFilterActive(postType)) POST(readRegularData(node \\ "a"))
      case TumblrTags.PHOTO => if (TumblrTags.isFilterActive(postType)) POST(readPhotoData(node \\ "a"))
      case TumblrTags.QUOTE => if (TumblrTags.isFilterActive(postType)) POST(readQuoteData(node \\ "a"))
      case TumblrTags.LINK => if (TumblrTags.isFilterActive(postType)) POST(readLinkData(node \\ "a"))
      case TumblrTags.CONVERSATION => if (TumblrTags.isFilterActive(postType)) POST(readConversationData(node \\ "a"))
      case TumblrTags.VIDEO => if (TumblrTags.isFilterActive(postType)) POST(readVideoData(node \\ "a"))
      case TumblrTags.AUDIO => if (TumblrTags.isFilterActive(postType)) POST(readAudioData(node \\ "a"))
      case TumblrTags.ANSWER => if (TumblrTags.isFilterActive(postType)) POST(readRegularData(node \\ "a", true))
      case _ => LOGGER.message("-- Unknown post type --")
    }
    //val metaLinks = node \\ "a"

  }

  def restore: Unit = {
    LOGGER.message("Restoring posts...")
    LOGGER.message("Interval between post (millisec): " + interval)
    backupNodes.reverse foreach { node =>
      postData(node)
      Thread.sleep(interval)
    }
    LOGGER.message("done!")
  }

}
