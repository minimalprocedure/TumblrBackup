/**
 * *****************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: zairik@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package org.pragmas.tumblr.application

import org.pragmas.tumblr._

object TumblrBackupApplication {
  def main(args: Array[String]): Unit = {

    Logger.out("Tumblr Backup ver. " + TumblrBackup.VERSION)
    Logger.out(TumblrBackup.COPYRIGHT)
    Logger.out("")
    Logger.out("start...")
    var start: Int = 0
    var num: Int = TumblrBackup.MAX_NUM

    args.size match {
      case 0 => Logger.out("tumblrBackup <tumblruser> <localfolder> <start> <num> [-v:--verbose]")
      case 1 => Logger.out("tumblrBackup " + args(0) + " <localfolder> <start> <num> [-v:--verbose]")
      case 2 => TumblrBackup.backupPublic(args(0), args(1), false)
      case 3 => {
        args(2) match {
          case "-v" | "-V" | "--verbose" => TumblrBackup.backupPublic(args(0), args(1), true)
          case _ => TumblrBackup.backupPublicByNum(args(0), args(1), args(2).toInt, 0, num, false)
        }
      }
      case 4 => {
        args(3) match {
          case "-v" | "-V" | "--verbose" => TumblrBackup.backupPublicByNum(args(0), args(1), args(2).toInt, 0, num, true)
          case _ => TumblrBackup.backupPublicByNum(args(0), args(1), args(2).toInt, args(3).toInt, num, false)
        }
      }
      case 5 => {
        args(4) match {
          case "-v" | "-V" | "--verbose" => TumblrBackup.backupPublicByNum(args(0), args(1), args(2).toInt, args(3).toInt, num, true)
          case _ => Logger.out("tumblrBackup <tumblruser> <localfolder> <start> <num> [-v:--verbose]")
        }
      }
      case _ => Logger.out("tumblrBackup <tumblruser> <localfolder> <start> <num> [-v:--verbose]")
    }
    Logger.out("")
    Logger.out("done...")
  }
}
