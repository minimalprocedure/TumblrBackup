/**
 * ****************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: zairik@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ***************************************************************************
 */

package org.pragmas.tumblr.application

import net.miginfocom.swt.MigLayout

import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.DirectoryDialog
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Shell
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.widgets.MessageBox
import org.eclipse.swt.widgets.Composite
import org.pragmas.tumblr._

import scala.collection.mutable.{ListBuffer}

object TumblrBackupApplicationGui {

  def main(args: Array[String]): Unit = {
    try {
      val window = new TumblrBackupApplicationGui
      window.open
    } catch { case e: Exception => e.printStackTrace }
  }

}

class TumblrBackupApplicationGui {

  val LOGGER = Logger

  private val APP_NAME = "Tumblr Backup ver. " + TumblrBackup.VERSION

  private val INVALIDVALUE = -1

  protected val shell: Shell = new Shell
  protected val migLayout = new MigLayout("", "[][][]", "")
  shell.setLayout(migLayout)

  protected var display: Display = _

  val chkBackup = new Button(shell, SWT.NONE | SWT.RADIO)
  chkBackup.setText("Backup")
  chkBackup.setSelection(true)
  chkBackup.setLayoutData("split 4")
  private val chkRestore = new Button(shell, SWT.NONE | SWT.RADIO)
  chkRestore.setText("Restore")
  chkRestore.setLayoutData("wrap")

  private val lblNickname = new Label(shell, SWT.NONE)
  lblNickname.setLayoutData("wmin 270, split 6")
  lblNickname.setText("Nickname")

  private val lblFromPostNum = new Label(shell, SWT.NONE)
  lblFromPostNum.setText("From num.")
  lblFromPostNum.setLayoutData("wmin 102")
  private val lblToPostNum = new Label(shell, SWT.NONE)
  lblToPostNum.setText("To num.")
  lblToPostNum.setLayoutData("wmin 102")
  private val lblMaxReqNum = new Label(shell, SWT.NONE)
  lblMaxReqNum.setText("Req num.")
  lblMaxReqNum.setLayoutData("wrap")

  private val textNickname: Text = new Text(shell, SWT.BORDER)
  textNickname.setLayoutData("wmin 240, grow, split 6")
  private val textFromPostNum: Text = new Text(shell, SWT.BORDER)
  textFromPostNum.setLayoutData("grow")
  private val textToPostNum: Text = new Text(shell, SWT.BORDER)
  textToPostNum.setLayoutData("grow, split 2")
  private val textMaxReqNum: Text = new Text(shell, SWT.BORDER)
  textMaxReqNum.setLayoutData("grow, split 2")
  textMaxReqNum.setText(TumblrBackup.MAX_NUM.toString)

  private val btnGetNumPosts = new Button(shell, SWT.NONE)
  btnGetNumPosts.setText("Num")
  btnGetNumPosts.setLayoutData("grow, wrap")

  private val lblLocalFolder = new Label(shell, SWT.NONE)
  lblLocalFolder.setText("Local Folder")
  lblLocalFolder.setLayoutData("wrap")
  private val textLocalfolder: Text = new Text(shell, SWT.BORDER)
  textLocalfolder.setLayoutData("wmin 240, grow,  split 2")

  private val btnLocalFolderChoice = new Button(shell, SWT.NONE)
  btnLocalFolderChoice.setText("Select")
  btnLocalFolderChoice.setLayoutData("wrap")

  private val postTypesComposite = new Composite(shell, SWT.NONE)
  postTypesComposite.setLayout(new MigLayout("", "[][][]", ""))
  private val chkRegular = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  chkRegular.setLayoutData("split 8")
  private val chkPhoto = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkQuote = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkLink = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkConversation = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkVideo = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkAudio = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  private val chkAnswer = new Button(postTypesComposite, SWT.NONE | SWT.CHECK)
  postTypesComposite.pack
  postTypesComposite.setLayoutData("wrap")

  private val imageSizeComposite = new Composite(shell, SWT.NONE)
  imageSizeComposite.setLayout(new MigLayout("", "[][][]", ""))
  private val lblImageSize = new Label(imageSizeComposite, SWT.NONE)
  lblImageSize.setText("Image size:")
  lblImageSize.setLayoutData("width 82!, split 4")
  private val chk1280 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  private val chk500 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  private val chk400 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  private val chk250 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  private val chk100 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  private val chk75 = new Button(imageSizeComposite, SWT.NONE | SWT.CHECK)
  //imageSizeComposite.pack
  imageSizeComposite.setLayoutData("wrap")

  private val submissionTypesComposite = new Composite(shell, SWT.NONE)
  submissionTypesComposite.setLayout(new MigLayout("", "[][][]", ""))
  private val chkPrivatePost = new Button(submissionTypesComposite, SWT.NONE | SWT.CHECK)
  chkPrivatePost.setText("private")
  chkPrivatePost.setSelection(true)
  val chkDraft = new Button(submissionTypesComposite, SWT.NONE | SWT.RADIO)
  chkDraft.setText("draft")
  chkDraft.setSelection(true)
  val chkPublished = new Button(submissionTypesComposite, SWT.NONE | SWT.RADIO)
  chkPublished.setText("published")
  val chkSubmission = new Button(submissionTypesComposite, SWT.NONE | SWT.RADIO)
  chkSubmission.setText("submission")
  submissionTypesComposite.pack
  submissionTypesComposite.setLayoutData("wrap")
  submissionTypesComposite.setVisible(false)

  private val lblLog = new Label(shell, SWT.NONE)
  lblLog.setText("Log")
  lblLog.setLayoutData("wrap")
  private val textLog: Text = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI)

  textLog.setLayoutData("height 100!,wmin 240, growx,  wrap")

  private val btnVerboseCheck = new Button(shell, SWT.CHECK)
  btnVerboseCheck.setText("Verbose")
  btnVerboseCheck.setSelection(true)
  btnVerboseCheck.setLayoutData("split 5")

  private val lblSpace1 = new Label(shell, SWT.NONE)
  lblSpace1.setText("")
  lblSpace1.setLayoutData("grow, split 3")

  private val btnAbout = new Button(shell, SWT.NONE)
  btnAbout.setText("About")
  //btnAbout.setLayoutData("grow, split 3")

  private val btnRunBackup = new Button(shell, SWT.NONE)
  btnRunBackup.setText("Backup")

  private val btnClose = new Button(shell, SWT.NONE)
  btnClose.setText("Close")

  private def setAbortButton(abort: Boolean = true): Unit = {
    Display.getDefault.syncExec(
      new Runnable() {
        override def run: Unit = {
          btnClose.setText(if (abort) "Abort" else "Close")
        }
      }
    )
  }

  private def setToNumField(num: Int): Unit = {
    Display.getDefault.syncExec(
      new Runnable() {
        override def run: Unit = {
          textToPostNum.setText(num.toString)
        }
      }
    )
  }

  private def setFilter(check: Button): Unit = {
    if (check.getSelection) TumblrTags.activeFilter(check.getText)
    else TumblrTags.deactiveFilter(check.getText)
  }

  private def setSize(check: Button): Unit = {
    if (check.getSelection) TumblrTags.activeSize(check.getText)
    else TumblrTags.deactiveSize(check.getText)
  }

  private def setFiltersBar: Unit = {

    chkRegular.setText(TumblrTags.REGULAR)
    chkRegular.setSelection(true)
    chkRegular.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkPhoto.setText(TumblrTags.PHOTO)
    chkPhoto.setSelection(true)
    chkPhoto.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkQuote.setText(TumblrTags.QUOTE)
    chkQuote.setSelection(true)
    chkQuote.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkLink.setText(TumblrTags.LINK)
    chkLink.setSelection(true)
    chkLink.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkConversation.setText(TumblrTags.CONVERSATION)
    chkConversation.setSelection(true)
    chkConversation.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkVideo.setText(TumblrTags.VIDEO)
    chkVideo.setSelection(true)
    chkVideo.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkAudio.setText(TumblrTags.AUDIO)
    chkAudio.setSelection(true)
    chkAudio.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chkAnswer.setText(TumblrTags.ANSWER)
    chkAnswer.setSelection(true)
    chkAnswer.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setFilter(e.getSource.asInstanceOf[Button])
      }
    })

    chk1280.setText(TumblrTags.SIZE_1280)
    chk1280.setSelection(true)
    chk1280.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
    chk500.setText(TumblrTags.SIZE_500)
    chk500.setSelection(true)
    chk500.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
    chk400.setText(TumblrTags.SIZE_400)
    chk400.setSelection(true)
    chk400.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
    chk250.setText(TumblrTags.SIZE_250)
    chk250.setSelection(true)
    chk250.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
    chk100.setText(TumblrTags.SIZE_100)
    chk100.setSelection(true)
    chk100.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
    chk75.setText(TumblrTags.SIZE_75)
    chk75.setSelection(true)
    chk75.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        setSize(e.getSource.asInstanceOf[Button])
      }
    })
  }

  private def enableBackup: Unit = {
    lblNickname.setText("Nickname")
    imageSizeComposite.setVisible(true)
    submissionTypesComposite.setVisible(false)
    lblFromPostNum.setText("From Post num.")
    lblToPostNum.setText("To Post num.")
    btnGetNumPosts.setVisible(true)
    lblImageSize.setEnabled(true)
    chk1280.setEnabled(true)
    chk500.setEnabled(true)
    chk400.setEnabled(true)
    chk250.setEnabled(true)
    chk100.setEnabled(true)
    chk75.setEnabled(true)
    btnRunBackup.setText("Backup")
    //shell.pack
  }

  private def enableRestore: Unit = {
    lblNickname.setText("Email")
    imageSizeComposite.setVisible(false)
    submissionTypesComposite.setVisible(true)
    lblFromPostNum.setText("Password")
    lblToPostNum.setText("Pause in sec.")
    btnGetNumPosts.setVisible(false)
    lblImageSize.setEnabled(false)
    chk1280.setEnabled(false)
    chk500.setEnabled(false)
    chk400.setEnabled(false)
    chk250.setEnabled(false)
    chk100.setEnabled(false)
    chk75.setEnabled(false)
    btnRunBackup.setText("Restore")
    //shell.pack
  }

  protected def createContents: Unit = {
    shell.setText(APP_NAME)
    shell.setBounds(SWT.DEFAULT, SWT.DEFAULT, SWT.DEFAULT, SWT.DEFAULT)

    chkBackup.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        enableBackup
      }
    })

    chkRestore.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        enableRestore
      }
    })

    setFiltersBar

    btnGetNumPosts.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        val nickname = textNickname.getText.stripMargin
        if (nickname != "") {
          new Thread(new Runnable() {
            def run: Unit = {
              try {
                val number = TumblrBackup.postNumber(nickname)
                setToNumField(number)
              } catch {
                case e: Exception => {
                  e.printStackTrace
                }
              }
            }
          }).start
        } else {
          val msgBox = new MessageBox(shell)
          msgBox.setMessage("Please insert nickname")
          msgBox.open
        }

      }
    })

    btnLocalFolderChoice.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        val dialog = new DirectoryDialog(shell, SWT.OPEN)
        val fileSelected = dialog.open()
        if (fileSelected != null) {
          textLocalfolder.setText(fileSelected)
        }
      }
    })

    btnAbout.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        val msgBox = new MessageBox(shell)
        msgBox.setMessage(APP_NAME + "\n" + TumblrBackup.COPYRIGHT)
        msgBox.open
      }
    })

    btnRunBackup.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        val verbose = btnVerboseCheck.getSelection
        Logger.GUILOGGERVIEWER = textLog
        if (chkBackup.getSelection) {
          val nickname = textNickname.getText.stripMargin
          val folder = if (textLocalfolder.getText.stripMargin != "") textLocalfolder.getText.stripMargin else "./tumblr-backup"
          val from = try { textFromPostNum.getText.stripMargin.toInt.abs } catch { case e: Exception => INVALIDVALUE }
          val to = try { textToPostNum.getText.stripMargin.toInt.abs } catch { case e: Exception => INVALIDVALUE }
          val num = try { textMaxReqNum.getText.stripMargin.toInt.abs } catch { case e: Exception => TumblrBackup.MAX_NUM }
          if (nickname != "" && folder != "") {
            new Thread(new Runnable() {
              def run: Unit = {
                try {
                  setAbortButton(true)
                  if (from != INVALIDVALUE && to != INVALIDVALUE)
                    TumblrBackup.backupPublicByNum(nickname, folder, from, to, num, verbose)
                  else
                    TumblrBackup.backupPublic(nickname, folder, verbose)
                  setAbortButton(false)
                } catch {
                  case e: Exception => {
                    e.printStackTrace
                    setAbortButton(false)
                  }
                }
              }
            }).start
          } else {
            val msgBox = new MessageBox(shell)
            msgBox.setMessage("Please insert al data requested")
            msgBox.open
          }
        } else {
          val nickname = textNickname.getText.stripMargin
          val password = textFromPostNum.getText.stripMargin
          val interval = try { textToPostNum.getText.stripMargin.toInt.abs * 1000 } catch { case e: Exception => 2000 }
          val folder = if (textLocalfolder.getText.stripMargin != "")
            textLocalfolder.getText.stripMargin
          else "./tumblr-backup"
          val privatePost = chkPrivatePost.getSelection
          val submissionType =
            if (chkDraft.getSelection) "draft"
            else if (chkPublished.getSelection) "published"
            else if (chkSubmission.getSelection) "submission"
            else "draft"
          if (nickname != "" && password != "" && folder != "") {
            new Thread(new Runnable() {
              def run: Unit = {
                try {
                  setAbortButton(true)
                  val restore = new TumblrRestore(nickname, password, privatePost, submissionType, folder, interval, verbose)
                  restore.restore
                  setAbortButton(false)
                } catch {
                  case e: Exception => {
                    e.printStackTrace
                    setAbortButton(false)
                  }
                }
              }
            }).start
          } else {
            val msgBox = new MessageBox(shell)
            msgBox.setMessage("Please insert al data requested")
            msgBox.open
          }
        }
      }
    })

    btnClose.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e: SelectionEvent): Unit = {
        try {
          display.dispose
          shell.close
        } catch {
          case e: Exception => LOGGER.error(e)
        } //?
      }
    })

    shell.pack
    val bounds = shell.getBounds
    shell.setMinimumSize(new Point(bounds.width, bounds.height))
    shell.open();
  }

  def open: Unit = {
    display = Display.getDefault
    createContents
    while (!shell.isDisposed) {
      if (!display.readAndDispatch) {
        display.sleep
      }
    }
  }

}
