/**
 * *****************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: zairik@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package org.pragmas.tumblr

import scala.collection.mutable.ListBuffer

object TumblrTags {

  val REGULAR = "regular"
  val PHOTO = "photo"
  val QUOTE = "quote"
  val LINK = "link"
  val CONVERSATION = "conversation"
  val VIDEO = "video"
  val AUDIO = "audio"
  val ANSWER = "answer"

  val CONTENT = "content"
  val TAGS = "tags"

  val SIZE_1280 = "1280"
  val SIZE_500 = "500"
  val SIZE_400 = "400"
  val SIZE_250 = "250"
  val SIZE_100 = "100"
  val SIZE_75 = "75"

  val FILTERS = ListBuffer[String](
    REGULAR,
    PHOTO,
    QUOTE,
    LINK,
    CONVERSATION,
    VIDEO,
    AUDIO,
    ANSWER
  )

  val SIZES = ListBuffer[String](
    SIZE_1280,
    SIZE_500,
    SIZE_400,
    SIZE_250,
    SIZE_100,
    SIZE_75
  )

  def activeFilter(text: String): Unit = {
    if (!FILTERS.contains(text)) FILTERS += text
  }

  def deactiveFilter(text: String): Unit = {
    if (FILTERS.contains(text)) FILTERS -= text
  }

  def isFilterActive(text: String): Boolean = {
    FILTERS.contains(text)
  }

  def activeSize(text: String): Unit = {
    if (!SIZES.contains(text)) SIZES += text
  }

  def deactiveSize(text: String): Unit = {
    if (SIZES.contains(text)) SIZES -= text
  }

  def isSizeActive(text: String): Boolean = {
    SIZES.contains(text)
  }

}
