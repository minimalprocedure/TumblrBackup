name := "TumblrBackup"

organization := "org.pragmas"

version := "1.0.0.0.beta"

scalaVersion := "2.11.11"

mainClass in assembly := Some("org.pragmas.tumblr.application.TumblrBackupApplicationGui")

fork in run := true

javaOptions in run ++= Seq("-Xmx1024m", "-Xss1m", "-XX:+UseConcMarkSweepGC", "-XX:+CMSClassUnloadingEnabled")

packageOptions in (Compile, packageBin) +=
Package.ManifestAttributes("Permissions" -> "all-permissions", "Application-Name" -> "Kojo")

resolvers += "maven-eclipse-repo" at "http://maven-eclipse.github.io/maven"

resolvers += "Apache Maven Repo" at "https://repository.apache.org/content/groups/public"

val os = (sys.props("os.name"), sys.props("os.arch")) match {
  case ("Linux", "amd64" | "x86_64") => "gtk.linux.x86_64"
  case ("Linux", _) => "gtk.linux.x86"
  case ("Mac OS X", "amd64" | "x86_64") => "cocoa.macosx.x86_64"
  case ("Mac OS X", _) => "cocoa.macosx.x86"
  case (os, "amd64") if os.startsWith("Windows") => "win32.win32.x86_64"
  case (os, _) if os.startsWith("Windows") => "win32.win32.x86"
  case (os, arch) => sys.error("Cannot obtain lib for OS '" + os + "' and architecture '" + arch + "'")
}

val swtArtifacts = "org.eclipse.swt." + os

libraryDependencies ++= Seq(
  "org.eclipse.swt" % swtArtifacts % "4.5.2",
  "com.miglayout" % "miglayout-swt" % "5.0",
  "commons-codec" % "commons-codec" % "1.4",
  "org.apache.commons" % "commons-lang3" % "3.4",
  "commons-logging" % "commons-logging" % "1.1.1",
  
  "org.apache.httpcomponents" % "httpclient" % "4.5.2",
  "org.apache.httpcomponents" % "httpclient-cache" % "4.5.2",
  "org.apache.httpcomponents" % "httpcore" % "4.4.4",
  "org.apache.httpcomponents" % "httpmime" % "4.5.2",
  
  "org.scala-lang" % "scala-library" % "2.11.11",
  "org.scala-lang" % "scala-compiler" % "2.11.11",
  "org.scala-lang" % "scala-reflect" % "2.11.11",
  "org.scala-lang" % "scala-actors" % "2.11.11",
  "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.6"
)
